#include<stdio.h>
struct bookdetails
{
 char title[50],author[30];
 float price;
 int pages;
};
int main()
{
 struct bookdetails b1,b2;
 printf("Enter details of first book:\nBook Title\nBook author\nBook price\nBook's number of pages\n");
 scanf("%s%s%f%d",b1.title,b1.author,&b1.price,&b1.pages);
 printf("Enter details of second book:\nBook Title\nBook author\nBook price\nBook's number of pages\n");
 scanf("%s%s%f%d",b2.title,b2.author,&b2.price,&b2.pages);
 if(b1.price>b2.price)
 printf("The details of the more expensive book are as follows\nBook Title:%s\nBook author:%s\nBook price:%f\nBook's number of pages:%d\n",b1.title,b1.author,b1.price,b1.pages);
 else if(b1.price<b2.price)
 printf("The details of the more expensive book are as follows\nBook Title:%s\nBook author:%s\nBook price:%f\nBook's number of pages:%d\n",b2.title,b2.author,b2.price,b2.pages);
 else printf("Both books are of the same price\n");
 if(b1.pages<b2.pages)
 printf("The details of the book having lower number of pages are as follows\nBook Title:%s\nBook author:%s\nBook price:%f\nBook's number of pages:%d\n",b1.title,b1.author,b1.price,b1.pages);
 else if(b1.pages>b2.pages)
 printf("The details of the book having lower number of pages are as follows\nBook Title:%s\nBook author:%s\nBook price:%f\nBook's number of pages:%d\n",b2.title,b2.author,b2.price,b2.pages);
 else printf("Both books have the same number of pages\n");
 return 0;
 }
 