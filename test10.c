#include<stdio.h>
int main()
{
  int a,b,*x,*y,add,sub,mul,rem;
  float div;
  x=&a;
  y=&b;
  printf("Enter any two integers\n");
  scanf("%d%d",x,y);
  //addition
  add=*x+*y;
  printf("Sum=%d\n",add);
  //subtraction
  sub=*x-*y;
  printf("Difference=%d\n",sub);
  //multiplication
  mul=(*x)*(*y);
  printf("Product=%d\n",mul);
  //division
  div=(float)(*x)/(*y);
  printf("%d divided by %d gives %f\n",*x,*y,div);
  //remainder
  rem=(*x)%(*y);
  printf("Remainder=%d\n",rem);
  return 0;
}