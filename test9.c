#include<stdio.h>
struct student
 {
  int rollno;
  char name[20];
  char section[5];
  char dept[5];
  float fees;
  int total;
 };
int main()
{
 struct student s1,s2;
 printf("Enter the details of the first student\n");
 scanf("%d%s%s%s%f%d",&s1.rollno,s1.name,s1.section,s1.dept,&s1.fees,&s1.total);
 printf("Enter the details of the second student\n");
 scanf("%d%s%s%s%f%d",&s2.rollno,s2.name,s2.section,s2.dept,&s2.fees,&s2.total);
 if(s1.total>s2.total)
 {
  printf("The details of the student who has scored more marks are\n");
  printf("Roll No.:%d\nName:%s\nSection:%s\nDepartment:%s\nFees:%f\nTotal Marks:%d\n",s1.rollno,s1.name,s1.section,s1.dept,s1.fees,s1.total);
 }
 else if(s1.total<s2.total)
 {
  printf("The details of the student who has scored more marks are\n");
  printf("Roll No.:%d\nName:%s\nSection:%s\nDepartment:%s\nFees:%f\nTotal Marks:%d\n",s2.rollno,s2.name,s2.section,s2.dept,s2.fees,s2.total);
 }
 else printf("Both the students have scored equal marks");
 return 0;
}
  